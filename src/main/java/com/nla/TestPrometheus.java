package com.nla;

import io.micrometer.core.instrument.Clock;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import io.micrometer.core.instrument.util.HierarchicalNameMapper;
import io.micrometer.jmx.JmxConfig;
import io.micrometer.jmx.JmxMeterRegistry;

import javax.management.*;
import java.lang.management.ManagementFactory;
import java.util.HashMap;
import java.util.Map;

public class TestPrometheus {


    // regarder MonitorRegister dans  note
    public TestPrometheus() throws Exception {

        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        ObjectName name = new ObjectName("com.nla:type=trade_out1");
        MyDynamicMBean mbean = new MyDynamicMBean();


        mbs.registerMBean(mbean, name);

        while(true) {
            Thread.sleep(1000);
            counterValue++;
        }

    }

    private static int counterValue = 0;
    // Implémentation de l'interface DynamicMBean pour exposer notre métrique
    public static class MyDynamicMBean implements DynamicMBean {
        @Override
        public Object getAttribute(String attribute) throws AttributeNotFoundException, MBeanException, ReflectionException {
            if ("Count".equals(attribute)) {
                return counterValue;
            }
            throw new AttributeNotFoundException("Attribute " + attribute + " not found");
        }

        @Override
        public void setAttribute(Attribute attribute) throws AttributeNotFoundException, InvalidAttributeValueException, MBeanException, ReflectionException {
            throw new AttributeNotFoundException("Setting attributes not supported");
        }

        @Override
        public AttributeList getAttributes(String[] attributes) {
            AttributeList list = new AttributeList();
            for (String attribute : attributes) {
                try {
                    list.add(new Attribute(attribute, getAttribute(attribute)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return list;
        }

        @Override
        public AttributeList setAttributes(AttributeList attributes) {
            return null;
        }

        @Override
        public Object invoke(String actionName, Object[] params, String[] signature) throws MBeanException, ReflectionException {
            throw new UnsupportedOperationException("No operations supported");
        }

        @Override
        public MBeanInfo getMBeanInfo() {
            Map<String, String> properties = new HashMap<>();
            properties.put("Count", "int");

            MBeanAttributeInfo[] attributes = new MBeanAttributeInfo[1];
            attributes[0] = new MBeanAttributeInfo("Count", "int", "Counter value", true, false, false);

            return new MBeanInfo(
                    getClass().getName(),
                    "My MBean",
                    attributes,
                    null,
                    null,
                    null
            );
        }
    }

    public static void main(String[] args) throws Exception {

        TestPrometheus test = new TestPrometheus();

    }


}
