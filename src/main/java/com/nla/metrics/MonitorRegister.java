package com.nla.metrics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

public class MonitorRegister {
    private static final Logger LOGGER = LoggerFactory.getLogger(MonitorRegister.class);

    public static MonitorService createMonitorMBean() {
        MonitorThread monitorThread = MonitorThread.getInstance();
        for(MonitorMXBean bean : monitorThread.getMBeans()) {
            register(bean);
        }
        return monitorThread;
    }

    private static void register(MonitorMXBean mbean) {
        String mbeanName = "com.nla:type=MonitorMetrics,name=" + mbean.getName();
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        try {
            ObjectName objectName = new ObjectName(mbeanName);
            try {
                mbs.registerMBean(mbean, objectName);
            } catch (InstanceAlreadyExistsException e) {
                LOGGER.warn(mbeanName + " already exists, find current");
            }
        } catch (Exception e) {
            LOGGER.error("Error when created MBean " + mbeanName, e);
        }
    }

}
