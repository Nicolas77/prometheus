package com.nla.metrics;

import javax.management.*;
import java.util.HashMap;

public class Monitor implements MonitorMXBean{

    private Metrics metrics;

    private String name;

    public Monitor(String name) {
        this.name = name;
        this.metrics = new Metrics(new HashMap<>());
    }

    @Override
    public Metrics getMetrics() {
        return this.metrics;
    }

    @Override
    public String getName() {
        return name;
    }


    @Override
    public Object getAttribute(String attribute) throws AttributeNotFoundException, MBeanException, ReflectionException {
        return null;
    }

    @Override
    public void setAttribute(Attribute attribute) throws AttributeNotFoundException, InvalidAttributeValueException, MBeanException, ReflectionException {

    }

    @Override
    public AttributeList getAttributes(String[] attributes) {
        return null;
    }

    @Override
    public AttributeList setAttributes(AttributeList attributes) {
        return null;
    }

    @Override
    public Object invoke(String actionName, Object[] params, String[] signature) throws MBeanException, ReflectionException {
        return null;
    }

    @Override
    public MBeanInfo getMBeanInfo() {
        return null;
    }
}
