package com.nla.metrics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class MonitorThread extends Thread implements  MonitorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MonitorThread.class);

    private static final MonitorThread INSTANCE = createAndStart();

    private BlockingQueue<MonitorMessage> queue = new ArrayBlockingQueue<>(1000);


    private MonitorThread() {
        this.setDaemon(true);
        this.setName("MonitorMBean");
        this.start();
    }

    private static MonitorThread createAndStart() {
        return new MonitorThread();
    }

    protected static MonitorThread getInstance() {
        return INSTANCE;
    }

    @Override
    public void run() {
        while (true) {
            try {
                MonitorMessage message = queue.poll(5000, TimeUnit.SECONDS);
                if(message == null) {
                    refreshTimeMonitorIfNeeded();
                } else {
                    switch (message.getMonitorAction()) {
                        case INCREMENT:
                            manageHandleCount(message.getHost(), message.getPort());
                            break;
                        case ERROR:
                            incrementErrorCount(message.getHost(), message.getPort());
                            break;
                        default:
                            LOGGER.warn("MBean - action not managed : " + message.getMonitorAction());
                    }
                }
            } catch (Exception e) {
                LOGGER.error("Error has occurred in MonitorMBean", e);
            }
        }
    }

    private void refreshTimeMonitorIfNeeded() {

    }

    public void manageHandleCount(String host, int port) {

    }

    public void incrementErrorCount(String host, int port) {
    }
    @Override
    public void messageSent(String host, int port, long durationMs) {
        this.queue.offer(MonitorMessage.createMessageAdd(host, port, durationMs));
    }

    public MonitorMXBean[] getMBeans() {
        MonitorMXBean[] monitorMXBeans = new MonitorMXBean[1];

        return monitorMXBeans;

    }
}
