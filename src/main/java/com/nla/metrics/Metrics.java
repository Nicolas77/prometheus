package com.nla.metrics;

import java.beans.ConstructorProperties;
import java.util.Map;

public class Metrics {

    public static final String ALL = "ALL";
    private final Map<String, Long> countByOutput;

    @ConstructorProperties({"countByDomain"})
    public Metrics(Map<String, Long> countByOutput) {
        this.countByOutput = countByOutput;
        this.countByOutput.put(ALL, 0L);
    }

    public Map<String, Long> getCountByOutput() {
        return countByOutput;
    }

    public void increment(String domain) {
        Long value = countByOutput.getOrDefault(domain, 0L);
        countByOutput.put(domain, ++value);
        countByOutput.put(ALL, countByOutput.compute(ALL, (k, v) -> v + 1L));
    }

}
