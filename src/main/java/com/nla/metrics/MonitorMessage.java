package com.nla.metrics;

public class MonitorMessage {

    public static MonitorMessage createMessageAdd(String host, int port, long durationMs) {
        return new MonitorMessage();
    }

    public String getHost() {
        return "";
    }

    public int getPort() {
        return 0;
    }

    public enum MonitorAction {
        INCREMENT,
        ERROR
    };

    private MonitorAction monitorAction;


    public MonitorAction getMonitorAction() {
        return monitorAction;
    }

    public void setMonitorAction(MonitorAction monitorAction) {
        this.monitorAction = monitorAction;
    }
}
