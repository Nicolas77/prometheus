package com.nla.metrics;

import javax.management.DynamicMBean;

public interface MonitorMXBean extends DynamicMBean {
    Metrics getMetrics();

    String getName();
}
