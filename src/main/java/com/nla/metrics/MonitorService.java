package com.nla.metrics;

import javax.management.DynamicMBean;

public interface MonitorService {
    void messageSent(String host, int port, long durationMs);
}
